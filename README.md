dictionary-cli is a basic word lookup CLI that allows users to quickly look up 
the definition of a word they've encountered in the wild, without having to
navigate to a dictionary website; just open up your command line.

This CLI uses the Oxford dictionary API, which requires an app_id and app_key for 
HTTP requests. You can register for these for free at http://https://developer.oxforddictionaries.com/

Commands: 

dictionary key
dictionary key set (sets your app_id and app_key)
dictionary key show (shows your current app_id and app_key config)
dictionary key remove (removes your current app_id and app_key config)
dictionary def (print out the definition of a word)

Options:

--limit