#!/usr/bin/env node

const { program } = require('commander');
const pkg = require('../package.json');
const { getDef } = require('../commands/def');

program.version(pkg.version)
       .command('key', 'Manage API key');

program.command('def')
       .description('Search for a word definition')
       .option('--limit <number>', 'Limits results to a specified number', 3)
       .action((cmd) => getDef(cmd));

program.parse(process.argv);