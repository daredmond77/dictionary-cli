// Required fields
const isRequired = input => input === '' ? 'API Key is required for app usage!' : true;

module.exports = { isRequired };