const Configstore = require('configstore');
const pkg = require('../package.json');

class KeyManager {
    constructor() {
        this.conf = new Configstore(pkg.name)
    }

    setKey(key) {
        this.conf.set('apiKey', key);
        return key;
    }

    setId(id) {
        this.conf.set('appId', id);
        return id;
    }

    getKey() {
        const key = this.conf.get('apiKey');

        if(!key) {
            throw new Error('No API Key found!');
        }

        return key;
    }

    getId() {
        const id = this.conf.get('appId');

        if(!id) {
            throw new Error('No app Id found!');
        }

        return id;
    }

    deleteKey() {
        const key = this.conf.get('apiKey');
        
        if(!key) {
            throw new Error('No API Key found!');
        }

        this.conf.delete('apiKey');
        return;
    }
}

module.exports = KeyManager;