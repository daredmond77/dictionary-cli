const axios = require('axios');
const colors = require('colors');

class DictionaryAPI {
    constructor(apiKey, appId) {
        this.appId = appId;
        this.apiKey = apiKey;
        this.baseUrl = 'https://od-api.oxforddictionaries.com/api/v2/entries/en-us/';
    }

    async getDefinition(word) {
        const options = {
            headers: {
                'app_id' : this.appId,
                'app_key' : this.apiKey
            }
        }
        try {
            const res = await axios.get(`${this.baseUrl}${word}?fields=definitions&strictMatch=true`, options);

            let mainDefinition = res.data.results[0].lexicalEntries[0].entries[0].senses[0].definitions[0];
            return mainDefinition;
        } catch (err) {
            console.error(err.message.red);
        }
    }
}

module.exports =  DictionaryAPI;

