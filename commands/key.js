const inquirer = require('inquirer');
const colors = require('colors');
const KeyManager = require('../lib/KeyManager');
const { isRequired } = require('../utils/validation');

const key = {
    async set() {
        const keyManager = new KeyManager();
        const questions = await inquirer.prompt([
            {
                type: 'input',
                name: 'id',
                message: 'Enter your app ID'.green,
                validate: isRequired
            },
            {
                type: 'input',
                name: 'key',
                message: 'Enter your API key'.green,
                validate: isRequired
            }
        ])
        const key = keyManager.setKey(questions.key);
        const id = keyManager.setId(questions.id);

        if(key && id) {
            console.log('Credentials set!'.blue);
        }
    },

    show() {
        try {
            const keyManager = new KeyManager();
            const key = keyManager.getKey();
            const id = keyManager.getId();

            console.log('Current app ID is: ' + id.yellow + '\nCurrent API key is: ' + key.yellow);

        } catch(err) {
            console.error(err.message.red);
        }
    },

    remove() {
        try {
            const keyManager = new KeyManager();
            keyManager.deleteKey();

            console.log('API Key successfully deleted'.red);


        } catch(err) {
            console.error(err.message.red);
        }
        


    }
}

module.exports = key;