const inquirer = require('inquirer');
const { isRequired } = require('../utils/validation');
const DictionaryAPI = require('../lib/DictionaryAPI');
const KeyManager = require('../lib/KeyManager');

const getDef = async (cmd) => {
    const input = await inquirer.prompt([
        {
            type: 'input',
            name: 'word',
            message: 'Which word would you like the definition of?',
            validate: isRequired
        }
    ])

    const word = input.word;

    try {

        const keyManager = new KeyManager();
        const key = keyManager.getKey();
        const id = keyManager.getId();
        const dictionaryAPI = new DictionaryAPI(key, id);
        const definition = await dictionaryAPI.getDefinition(word);
        console.log('\n\t' + definition.yellow + '\n');
        
    } catch (err) {
        console.log(err.message.red);
    }

    

    

    
    
}

module.exports = { getDef };

